# AWBWreplay

This addon will automatically cycle through AWBW game replays.

## What it does

This extension gives the user the ability to choose the extent of the replay and have the program cycle through the replay automatically.

## Why

Saves time  and effort over manual execution of replays mid-game and allows for a full replay with animations to be viewed and captured post-game.

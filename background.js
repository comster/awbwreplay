// Set the default icon when the extension loads
browser.browserAction.setIcon({path: "icons/os.png"});
// Listen for message from content script and respond
browser.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.siteLoaded) {
    console.log("Event 1A: Message received, siteLoaded = true");
    browser.tabs.query({active: true, currentWindow: true}).then(function(tabs) {
      // Check if the active tab matches the specified site
      if (tabs[0].url.includes("awbw.amarriner.com/2030.php")) {
        // Set the icon to the active state when the user is on the specified site
        browser.browserAction.setIcon({path: "icons/ge.png", tabId: tabs[0].id});
        console.log("Event 1B: Icon set to Green");
      }
    });
  } else {
    console.log("Event 2A: Message received, siteLoaded = false");
    browser.browserAction.setIcon({path: "icons/os.png"});
    console.log("Event 2B: Icon set to Red");
  }
});

// Create a popup menu
browser.browserAction.onClicked.addListener(() => {
  browser.windows.create({
    type: "popup",
    url: "popup.html",
    width: 200,
    height: 250,
  });
});

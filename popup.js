// Wait for popup.html to finish loading
document.addEventListener("DOMContentLoaded", function() {
  // Pick out the fourth radio button option (Custom)
  const radio4 = document.querySelector('#option4');
  // Set listener for a click on that radio button
  radio4.addEventListener("click", function() {
    console.log("Radio clicked:");
    // Send request to content script for the list of turns from the replay selector
    browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      browser.tabs.sendMessage(tabs[0].id, { action: "getSelectOptions" }, function (response) {
        console.log("Replay Selector Data:");
        console.log(response);
        const dd1 = document.getElementById("dropdown1");
        const dd2 = document.getElementById("dropdown2");

        // Expand dropdown element options to include the selector data
        for (let i = 0; i < response.options.length; i++) {
          const option1 = document.createElement("option");
          const option2 = document.createElement("option");

          option1.value = response.options[i].value;
          option1.text = response.options[i].text;

          option2.value = response.options[i].value;
          option2.text = response.options[i].text;

          dd1.appendChild(option1);
          dd2.appendChild(option2);
        }
      });
    });
  });
});

// Send message to content script to open
browser.tabs.query({ active: true, currentWindow: true }, function (tabs) {
  browser.tabs.sendMessage(tabs[0].id, { action: "OpenReplay" }, function (response) {
    console.log("Response received:");
    console.log(response);
  });
});

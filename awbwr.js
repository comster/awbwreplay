// Send a message to the background script when the page loads the site
if (window.location.href.includes("awbw.amarriner.com/2030.php")) {
  browser.runtime.sendMessage({siteLoaded: true});
  console.log("Event 1: Site Loaded");
}

// Send a message to the background script when the user unloads (i.e. leaves) the site tab
window.addEventListener("beforeunload", function() {
  if (window.location.href.includes("awbw.amarriner.com/2030.php")) {
    browser.runtime.sendMessage({siteLoaded: false});
    console.log("Event 2: Site Unloaded");
  }
});

// Get the open replay button element
const openreplayButton = document.querySelector('.replay-open');

function openButton() {
    openreplayButton.click();
    console.log('Open Replay Button Activated');
}

// Get the replayforward button element
const forwardreplayButton = document.querySelector('.replay-forward-action');

function forwardButton() {
    forwardreplayButton.click();
    console.log('Replay Action Forward Activated');
}

//Retreive the turn data from the replay selector
function getSelectOptions() {
  let selectField = document.querySelector(".replay-day-selector select");
  let options = selectField.options;

  const selectOptions = [];

  for (let i = 0; i < options.length; i++) {
    selectOptions.push({
      value: options[i].value,
      text: options[i].text,
    });
  }

  return selectOptions;
}

//Add listener for data request from popup.js and set to return data from getSelectOptions
browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === "OpenReplay") {
    const replayopenmessage = "Replay was opened";
    openButton();
    sendResponse({ options: replayopenmessage });
  }
  if (request.action === "getSelectOptions") {
    const selectOptions = getSelectOptions();
    sendResponse({ options: selectOptions });
  }
});

/*
Draw a border round the document.body to test
*/
document.body.style.border = "5px solid red";
